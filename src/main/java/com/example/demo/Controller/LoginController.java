package com.example.demo.Controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Model.Account;
import com.example.demo.Service.LoginService;

@Controller
public class LoginController {
	
	@Autowired
	LoginService loginService;
	
	@RequestMapping(path="/")
	public ModelAndView login() {
		ModelAndView mav = new ModelAndView("index");
		mav.addObject("message",null);
		return mav;
	}
	
	@RequestMapping(path="/login")
	public ModelAndView login(@RequestParam Map<String,String> request) {
		ModelAndView mav = new ModelAndView();
		return loginService.response(mav , request);
	}
}
