package com.example.demo.Service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Model.Account;
import com.example.demo.Model.Login;
import com.example.demo.Repository.AccountRepository;
import com.example.demo.Repository.LoginRepository;

@Service
public class LoginService {

	@Autowired
	LoginRepository loginRepository;
	
	@Autowired
	AccountRepository accountRepository;
	
	public ModelAndView response(ModelAndView mav , Map<String,String> request) {
		Login login = loginRepository.onLogin(request.get("username"), request.get("password"));
		
		Account acc = new Account();
		if(login != null) {
			acc = accountRepository.onGetAccount(request.get("username"));
			mav.addObject("account",acc);
			mav.addObject("message","เข้าสู่ระบบสำเร็จ");
			mav.setViewName("main");
		}else {
			mav.setViewName("index");
			mav.addObject("message","ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้องกรุณาลองใหม่อีกครั้ง");
		}
		
		return mav;
	}
}
