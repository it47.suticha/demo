package com.example.demo.Repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Model.Account;
import com.example.demo.Model.Login;

@Repository
public interface LoginRepository extends CrudRepository<Login,Long> {

	@Query(value="select * from login l where l.username=?1 and l.password = ?2", nativeQuery = true)
	Login onLogin(String username, String password);
	
	@Query(value="select * from account a where a.username=?1",nativeQuery = true)
	Account getAccount(String username);
}
