package com.example.demo.Repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Model.Account;
import com.example.demo.Model.Login;

@Repository
public interface AccountRepository extends CrudRepository<Account,Long> {
	
	@Query(value="select * from account a where a.username = ?1", nativeQuery = true)
	Account onGetAccount(String username);

}
